function renderMovies(results) {
	console.log(results);
	movieList.render(results.movies)	
}

var movieList = {
	template: Handlebars.compile($('#movie-template').html()),

	container: document.getElementById('js-movie-results'),

	setLoading: function() {
		this.container.innerHTML = 'Loading...';
	},

	render: function(movies) {
		var html = '';

		for (var i = 0; i < movies.length; i++) {
			html += this.template(movies[i]);
		}

		this.container.innerHTML = html;
	}
};

var http = {
	jsonp: function(url) {
		var script = document.createElement('script');
		script.src = url;
		document.getElementsByTagName('head')[0].appendChild(script);
	}
};

var rottenTomatoes = {
	search: function(term) {
		var url = 'http://api.rottentomatoes.com/api/public/v1.0/movies.json';
		var params = {
			q: term,
			page_limit: 40,
			page: 1,
			apikey: 'r7dff6zkcr7mepq83k8egepf',
			callback: 'renderMovies'
		};

		http.jsonp(url + '?' + $.param(params));
	}
};

$('.js-movie-search-form').on('submit', function(e) {
	var searchTerm = $(this).find('.js-search-term').val();

	e.preventDefault();
	console.log('Searching for: ' + searchTerm);

	movieList.setLoading();
	rottenTomatoes.search(searchTerm);
});
